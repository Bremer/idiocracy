#!/usr/bin/env python3
import random
import time


class Player:
    def __init__(self, guesses, time, name):
        self.guesses = guesses
        self.time = time
        self.name = name


class Game:
    def __init__(self):
        self.players = []


    def start(self):
        self.cont = True
        self.game = True
        self.new_game = True
        self.test = True
        self.guesses = 0
        self.guess = 0
        self.start_time = time.time()
        self.number = random.randint(0, 100)
        self.idiocracy_guesses = 7
        self.idiocracy_turns = 7


    def stop_timer(self):
        self.stop_time = time.time()
        self.total_time = self.stop_time - self.start_time


    def idiocracy(self):
        self.idiocracy_guesses = self.idiocracy_guesses - 1
        if self.idiocracy_guesses > 0:
            print(f'You have {self.idiocracy_guesses} guesses left before regeneration of random number')
        else:
            self.number = random.randint(0, 100)
            self.idiocracy_turns = self.idiocracy_turns - 1
            self.idiocracy_guesses = self.idiocracy_turns
            print(f'New number has been generated, you have {self.idiocracy_guesses} guesses left.')
        if self.idiocracy_turns == 0:
            print('GAME OVER: You failed to guess the right number')
            self.cont = False
            self.game = False
            self.new_game = False
            self.test = False


def main():
    game = Game()
    while True:
        game.start()
        print("Welcome to idiocracy!")
        print("Guess a number between 0-100")
        while game.test:
            while game.cont:
                while game.new_game:
                    print(game.number)
                    game.idiocracy()
                    if game.new_game == False:
                        break
                    try:
                        game.guess = int(input(">"))
                        game.game = True
                    except Exception:
                        print("Only numbers!")
                        game.game = False
                    while game.game:
                        game.guesses = game.guesses + 1
                        if game.guess == game.number:
                            game.game = game.test = game.cont = False
                            game.stop_timer()
                            print("Correct number!")
                            print("Enter your name:")
                            player_name = input(">")
                            player = Player(game.guesses, game.total_time, player_name)
                            game.players.append(player)
                            game.new_game = False
                            break
                        elif game.guess < game.number:
                            print("To low!")
                            break
                        elif game.guess > game.number:
                            print("Too high!")
                            break
        print("Play again?")
        again = input(">")
        if again[0] == "n" or again[0] == "N":
            break
    sorted_players = bubble_sort(game.players)
    print_highscore(sorted_players)


def bubble_sort(players):
    list_length = len(players)
    for j in range(list_length):
        for i in range(list_length):
            if (i + 1) < list_length:
                if players[i].guesses > players[i+1].guesses:
                    temp = players[i]
                    players[i] = players[i+1]
                    players[i+1] = temp
                if players[i].guesses == players[i+1].guesses and players[i].time > players[i+1].time:
                    temp = players[i]
                    players[i] = players[i+1]
                    players[i+1] = temp
    return players


def print_highscore(sorted_players):
    print("****HIGHSCORE****")
    print("==================")
    for item in sorted_players:
            player = f'{item.name:10} {item.guesses:01} {item.time:.2f}'
            print(player)


if __name__ == "__main__":
    main()
